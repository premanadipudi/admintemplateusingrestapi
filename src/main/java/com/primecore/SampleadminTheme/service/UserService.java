package com.primecore.SampleadminTheme.service;

import java.util.List;

import com.primecore.SampleadminTheme.model.User;

public interface UserService {
	public List<com.primecore.SampleadminTheme.model.User> getAllUsers();
	 
	 public User getUserById(long id);
	 
	 public void saveOrUpdate(User user);
	 
	 public void deleteUser(long id);
}
