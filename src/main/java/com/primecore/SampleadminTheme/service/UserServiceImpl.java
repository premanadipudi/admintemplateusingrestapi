package com.primecore.SampleadminTheme.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.primecore.SampleadminTheme.dao.testUser;
import com.primecore.SampleadminTheme.model.User;



@Service
@Transactional
public class UserServiceImpl implements UserService {
 
 @Autowired
 testUser tuser;

 @Override
 public List<User> getAllUsers() {
  return (List<User>) tuser.findAll();
 }

 @Override
 public User getUserById(long id) {
  return tuser.findById((int) id).get();
 }

 @Override
 public void saveOrUpdate(User user) {
	 tuser.save(user);
	 }

 @Override
 public void deleteUser(long id) {
	 tuser.deleteById((int) id);
 }

}