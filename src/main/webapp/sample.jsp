<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>   
 
<jsp:include page="header.jsp"></jsp:include>
<div class="content-main " id="content-main">
                    <!-- ############ Main START-->
                    <div class="padding">
                        
                        <div id="toolbar">
                        <spring:url value="/user/addUser/" var="addURL" />
                            <!-- <button id="remove" class="btn btn-fw info" disabled> -->
                            <a class="btn btn-fw info" href="${addURL }" role="button" ><i class="fa fa-fw fa-plus"></i>Add New User</a>
 
                               <!--  <i class="fa fa-fw fa-plus" href="addURL"></i> Add User
                            </button> -->
                        </div>
                        <br/>
                        <div class="box">
                            <table id="table" data-toolbar="#toolbar" data-search="true" data-show-columns="true" data-show-export="true" data-detail-view="false" data-mobile-responsive="true" data-pagination="true" data-page-list="[10, 25, 50, 100, ALL]"
                            class="table"><tbody>
				<tr>
					<th scope="row">ID</th>
					<th scope="row">FirstName</th>
					<th scope="row">LastName</th>
					<th scope="row">Email</th>
					<th scope="row">Update</th>
					<th scope="row">Delete</th>

				</thead></tr>
				<c:forEach items="${userList }" var="user">
					<tr>
						<td>${user.id }</td>
						<td>${user.firstname}</td>
						<td>${user.lastname}</td>
						<td>${user.email}</td>
						<td><spring:url value="/user/updateUser/${user.id }"
								var="updateURL" /> <a class="btn btn-primary"
							href="${updateURL }" role="button">Update</a></td>
						<td><spring:url value="/user/deleteUser/${user.id }"
								var="deleteURL" /> <a class="btn btn-primary"
							href="${deleteURL }" role="button">Delete</a></td>
					</tr>
				</c:forEach></tbody>
			</table>
                        </div>
                    </div>
                    <!-- ############ Main END-->
                </div>
<!-- heloooo welcome to my sample page!!!!!!!!!! -->
<jsp:include page="footer.jsp"></jsp:include>



