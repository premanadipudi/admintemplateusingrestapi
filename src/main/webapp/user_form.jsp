
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>



<jsp:include page="header.jsp" />
<div class="padding">
	<spring:url value="/user/saveUser" var="saveURL" />
	<h2>User</h2>
	<form:form modelAttribute="userForm" method="post" action="${saveURL}" data-plugin="parsley" data-option="{}">
		<form:hidden path="id" />
		<div class="form-group row">
			<label class="col-sm-2 col-form-label">FirstName</label>
			<div class="col-sm-10">
				<form:input type="text" path="firstname" class="form-control"
					id="firstname" required="required" autocomplete="off"/>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 col-form-label">LastName</label>
			<div class="col-sm-10">
				<form:input type="text" path="lastname" class="form-control"
					id="lastname" required="required" autocomplete="off"/>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 col-form-label">Email</label>
			<div class="col-sm-10">
				<form:input type="text" path="email" class="form-control"
					id="email" required="required" autocomplete="off"/>
			</div>
		</div>
		<div>
			<button type="submit" class="btn btn-fw info">Save</button>
		</div>
	</form:form>

</div>
<jsp:include page="footer.jsp"></jsp:include>